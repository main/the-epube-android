package org.fox.epube;

import android.os.Bundle;

import com.livefront.bridge.Bridge;
import com.livefront.bridge.SavedStateHandler;

import java.net.CookieHandler;
import java.net.CookieManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import icepick.Icepick;

public class Application extends android.app.Application {
    private static Application m_singleton;

    @Override
    public final void onCreate() {
        super.onCreate();

        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        Bridge.initialize(getApplicationContext(), new SavedStateHandler() {
            @Override
            public void saveInstanceState(@NonNull Object target, @NonNull Bundle state) {
                Icepick.saveInstanceState(target, state);
            }

            @Override
            public void restoreInstanceState(@NonNull Object target, @Nullable Bundle state) {
                Icepick.restoreInstanceState(target, state);
            }
        });

        m_singleton = this;
    }

    public static Application getInstance(){
        return m_singleton;
    }


}
